<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return view('master');
    }

    public function login(){
        return view('user.login');
    }
}
