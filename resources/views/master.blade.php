<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Game Center</title>
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    
    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('anime-main/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/style.css')}}" type="text/css">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.header')
    <!-- Header End -->

    @yield('content')

<!-- Footer Section Begin -->
    @include('partial.footer')
  <!-- Footer Section End -->

<!-- Js Plugins -->
<script src="{{asset('anime-main/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('anime-main/js/bootstrap.min.js')}}"></script>
<script src="{{asset('anime-main/js/player.js')}}"></script>
<script src="{{asset('anime-main/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('anime-main/js/mixitup.min.js')}}"></script>
<script src="{{asset('anime-main/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('anime-main/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('anime-main/js/main.js')}}"></script>


</body>

</html>